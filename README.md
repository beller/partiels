**Partiels** offers a dynamic and ergonomic interface to **analyze** audio files, **load** data files, **organize** and **visualize** the analyses within groups, and **export** the results as images or text files. The application is intended for signal processing researchers, musicologists, composers, and sound designers. The results can be used in other applications such as *Max*, *Pure Data*, *Open Music*, etc.

![Partiels-Overview](https://git.forum.ircam.fr/beller/partiels/uploads/039f461da7d3ed0e6fb9be54a7c926a9/Partiels-v1.0.7-actions.gif)  

• Versions for *Windows*, *Mac* & *Linux*  
• Multiformat & multichannel support  
• Multifile support (useful to compare audio files)  
• Analyzing audio with *Vamp* plug-ins  
• Visualizing results as *spectrogram*, *lines*, and *markers*  
• Drawing and editing results (copy/cut/paste/delete/duplicate/etc.)  
• Organizing and ordering analyses in groups  
• Exporting results to *PNG*, *JPEG*, *CSV*, *JSON*, *CUE* & *SDIF* file formats  
• Loading results from *CSV*, *JSON*, *CUE* & *SDIF* file formats  
• Converting results from *SDIF* to *JSON* file formats and inversely  
• Batch processing (useful to apply the same set of analyses on several audio files)  
• Command line interface to analyze, export and convert results  
• Consolidating documents (useful to share documents/analysis with other users across different platforms)  
• Audio playback with loop  

Alongside Partials, you will find a *collection of Vamp analysis plug-ins* inspired by the AudioSculpt application. Vamp is an audio processing plugin system that extracts descriptive information from audio data. 
  
<details><summary>SuperVP</summary><ul>
<li>Spectrogram - Fast Fourier Transform</li>
<li>Spectrogram - Linear Predictive Coding</li>
<li>Spectrogram - True Envelope</li>
<li>Spectrogram - Reassigned Spectrum</li>
<li>Spectrogram - Cepstrum</li>
<li>Pitch Estimation - Feature Scoring</li>
<li>Pitch Estimation - Primes Matching</li>
<li>Pitch Estimation - Pitched Percussion</li>
<li>Markers - Transient Detection</li>
<li>Markers - Spectral Difference Positive</li>
<li>Markers - Spectral Difference Negative</li>
<li>Other - Peak</li>
<li>Other - Voiced / Unvoiced</li>
<li>Other - Masking Effects</li>
<li>Other - Formant LPC</li>
<li>Other - Formant CED</li>
<li>Other - Rd Shape</li>
</ul></details>
<details><summary>IrcamBeat</summary><ul>
<li>Beat Marker Detection</li>
<li>Mean Tempo Estimation</li>
</ul></details>
<details><summary>IrcamDescriptor</summary><ul>
<li>Perceptual - Loudness</li>
<li>Perceptual - Sharpness</li>
<li>Spectral - Centroid</li>
<li>Spectral - Decrease</li>
<li>Spectral - Rolloff</li>
<li>Spectral - Spread</li>
</ul></details>
<details><summary>PM2</summary><ul>
<li>Partial Tracking Harmonic</li>
<li>Partial Tracking Inharmonic</li>
</ul></details>  
  
</br>
  
![Partiels-Compatibility](https://git.forum.ircam.fr/beller/partiels/uploads/0a3e823c503bf57f08139d35f7fb0e4e/Logos.png)   
Partiels and the analysis plug-ins collection are compatible with **macOS** 10.13 and higher 64bit (Universal 2 - Intel/Silicon), **Windows** 10 & 11 64bit and **Linux** 64bit. 

The Partiels applications and the analysis plug-ins collection are part of the Ircam [Forum Premium](https://forum.ircam.fr/about/offres-dabonnement/) technologies bundle offering [many tools, plug-ins, applications, and datasets](https://forum.ircam.fr/collections/detail/Technologies-ircam-premium/) to analyze, synthesize, and transform the sound. The Premium subscription gives you access to **downloading and installing Ircam Premium technologies for one year**. Once installed on your machine, **you can use these technologies without time limit**. The [free membership](https://forum.ircam.fr/collections/detail/technologies-ircam-free/) allows you to use the **demo version** of the Partiels application and the analysis plug-ins collection.

If you have any **requests**, **comments**, or **bug reports**, please create a new thread on the [discussion forum](https://discussion.forum.ircam.fr/).

[comment]: <![Partiels-Com](https://git.forum.ircam.fr/beller/partiels/uploads/a2d4aaf07822d6028b7026a5754018ea/Communication.png)>  

**Partiels** is designed and developed by Pierre Guillot at [IRCAM](https://www.ircam.fr/) IMR Department.  
**Ircam Vamp Plug-ins** are designed and developed by Pierre Guillot and Matthew Harris at [IRCAM](https://www.ircam.fr/) IMR Department.  
**SuperVP** is designed by Axel Röbel (based on an initial version by Philippe Depalle) and developed by Axel Röbel and Frédéric Cornu of the [Analysis-Synthesis team](https://www.stms-lab.fr) of the STMS Lab hosted at IRCAM.  
**IrcamBeat** is designed by Geoffroy Peeters and developed by Geoffroy Peeters and Frédéric Cornu of the [Analysis-Synthesis team](https://www.stms-lab.fr) of the STMS Lab hosted at IRCAM.  
**IrcamDescripor** is designed and developed by [Analysis-Synthesis team](https://www.stms-lab.fr) of the STMS Lab hosted at IRCAM.  
**PM2** is designed and developed by [Analysis-Synthesis team](https://www.stms-lab.fr) of the STMS Lab hosted at IRCAM.  
**Vamp** is designed and developed at the Centre for Digital Music, Queen Mary, University of London: [www.vamp-plugins.org](https://www.vamp-plugins.org/).  

[**Video tutorials collection**](https://forum.ircam.fr/article/detail/tutoriel-partiels/)  
[![Introduction](https://git.forum.ircam.fr/beller/partiels/uploads/3ede8429364e2c953c8f49f74abb38ea/Video-Vignette.png)](https://nubo.ircam.fr/index.php/s/WMjMsAFCb3XWFFP)

  

